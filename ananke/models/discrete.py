import functools
import logging
from collections import ChainMap

import numpy as np
from pgmpy.factors.discrete import DiscreteFactor, TabularCPD
from pgmpy.inference import VariableElimination
from pgmpy.models import BayesianNetwork

from ..graphs import ADMG, DAG

logger = logging.getLogger(__name__)


def generate_random_cpds(graph, dir_conc=10, context_variable="S"):
    """
    Given a graph and a set of cardinalities for variables in a DAG, constructs random conditional probability distributions. Supports optional contexts and context variable to generate CPDs consistent with a context specific DAG for data fusion.

    :param graph: A graph whose variables have cardinalities, and optionally
    :type Graph: DAG, ADMG, LSADMG
    :param dir_conc: The Dirichlet concetration parameter
    :param context_variable: Name of the context variable
    """

    cpds = list()
    cards = {v: graph.vertices[v].cardinality for v in graph.vertices}
    for k, v in cards.items():
        if v is None:
            raise ValueError(
                "Invalid cardinality provided for vertex {}".format(k)
            )
    if hasattr(graph, "contexts"):
        contexts = list(graph.contexts.keys())
        context_distributions = list(graph.contexts.values())
    else:
        contexts = None

    for vertex in graph.vertices:
        parents = list(graph.parents(vertex))

        if context_variable not in parents or contexts is None:
            if not graph.parents(vertex):
                values = np.random.dirichlet(
                    cards[vertex] * [dir_conc],
                    1,
                ).T
                cpd = TabularCPD(
                    variable=vertex, variable_card=cards[vertex], values=values
                )

            else:
                values = np.random.dirichlet(
                    cards[vertex] * [dir_conc],
                    np.prod([cards[x] for x in parents]),
                ).T

                cpd = TabularCPD(
                    variable=vertex,
                    variable_card=cards[vertex],
                    values=values,
                    evidence=parents,
                    evidence_card=[cards[x] for x in parents],
                )

        else:
            no_s_parents = parents.copy()
            no_s_parents.remove(context_variable)
            no_s_parents = list(no_s_parents)
            reordered_parents = [context_variable] + no_s_parents
            s_specific_values = np.random.dirichlet(
                cards[vertex] * [dir_conc],
                int(np.prod([cards[x] for x in no_s_parents])),
            ).T
            values = []
            for i, context in enumerate(contexts):
                if vertex in set(context):
                    ix = context.index(vertex)
                    distribution = context_distributions[i][ix]
                    intervened_values = np.tile(
                        distribution,
                        (np.prod([cards[x] for x in no_s_parents]), 1),
                    ).T
                    values.append(intervened_values)
                else:
                    values.append(s_specific_values)
            values = np.hstack(values)
            cpd = TabularCPD(
                variable=vertex,
                variable_card=cards[vertex],
                values=values,
                evidence=reordered_parents,
                evidence_card=[cards[x] for x in reordered_parents],
            )

        cpds.append(cpd)

    return cpds


def generate_bayesian_network(graph, cpds):
    """
    Creates a Bayesian Network from Ananke graph and a list of pgmpy TabularCPDs.

    :param graph: Graph
    :param cpds: A list of conditional probability distributions consistent with 'graph'
    :type cpds: List[TabularCPD]

    """

    net = BayesianNetwork()
    net.add_nodes_from(graph.vertices)
    net.add_edges_from(graph.di_edges)

    net.add_cpds(*cpds)

    return net


def intervene(net, treatment_dict):
    """
    Performs an intervention on a pgmpy.models.BayesianNetwork, by setting the conditional distribution of
    each intervened variable to be a point mass at the intervened value. Does not alter the structure of the parents of the network (i.e. is a non-faithful operation).


    :param net: Bayesian Network
    :type net: pgmpy.models.BayesianNetwork
    :param treatment_dict: dictionary of variables to values:
    :type treatment_dict: dict


    """
    net_copy = net.copy()

    for vertex, value in treatment_dict.items():
        old_cpd = net_copy.get_cpds(vertex)
        old_values = old_cpd.get_values()
        new_values = np.zeros(old_values.shape)
        new_values[value, :] = 1
        new_cpd = TabularCPD(
            variable=vertex,
            variable_card=old_cpd.variable_card,
            values=new_values,
            evidence=old_cpd.variables[1:],
            evidence_card=old_cpd.cardinality[1:],
        )
        net_copy.add_cpds(new_cpd)

    return net_copy


def estimate_effect_from_discrete_dist(oid, net, treatment_dict, outcome_dict):
    """
    Performs the ID algorithm to identify a causal effect given a discrete
    probability distribution representing the observed data distribution.

    :param oid: Ananke OneLineID object
    :type oid: OneLineID
    :param net: pgmpy.BayesianNetwork-like object
    :param treatment_dict: dictionary of treatment variables and values
    :param outcome_dict: dictionary of outcome variables and values
    """
    if not oid.id():
        return None

    factors = list()
    for district in sorted(oid.Gystar.districts):
        fixing_order = oid.fixing_orders[tuple(district)]
        computed_factor = compute_district_factor(oid.graph, net, fixing_order)
        factors.append(computed_factor)

    intervened_distribution = functools.reduce(
        (lambda first, last: first.product(last, inplace=False)), factors
    )

    # construct the variable tuples here using itertools.product (Y* - Y) with A = a
    summed_vars = oid.ystar - set(outcome_dict)

    # compute the causal effect

    causal_effect = intervened_distribution.marginalize(
        summed_vars, inplace=False
    ).get_value(**dict(ChainMap(treatment_dict, outcome_dict)))

    return causal_effect


def compute_district_factor(graph, net, fixing_order):
    """
    Compute the interventional distribution associated with a district (or equivalently, its fixing order)

    :param graph: Graph representing the problem
    :type graph: ananke.ADMG
    :param net: Probability distribution corresponding to the graph
    :type net: pgmpy.models.BayesianNetwork
    :param fixing_order: A fixing sequence for the implied district D
    """
    inference = VariableElimination(net)
    new_graph = graph.copy()
    curr_factor = inference.query(graph.vertices)
    # curr_factor = margin
    for var in fixing_order:
        non_descendants = list(
            set(new_graph.vertices) - set(new_graph.descendants(var))
        )
        non_div_vars = set(new_graph.descendants(var)) - {var}
        div_joint = curr_factor.marginalize(non_div_vars, inplace=False)
        if non_descendants:
            div_cond = div_joint.marginalize([var], inplace=False)
            div_factor = div_joint.divide(div_cond, inplace=False)
        else:
            div_factor = div_joint
        new_graph = new_graph.fix(var)

        curr_factor = curr_factor.divide(div_factor, inplace=False)

    return curr_factor


def compute_effect_from_discrete_model(net, treatment_dict, outcome_dict):
    """
    Compute the causal effect by directly performing an intervention in a Bayesian
    Network corresponding to the true structural equation model to obtain the
    counterfactual distribution, and then computing the marginal distribution of the outcome.
    Note that this function does not consider issues of identification as
    interventions are performed in the true model (regardless if those
    interventions were identified).

    :param net: A Bayesian Network representing the causal problem. Note that this object is used only as a representation of the observed data distribution.
    :param treatment_dict: Dictionary of treatment variables to treatment values.
    :param outcome_dict: Dictionary of outcome variables to outcome values.
    """

    int_net = intervene(net, treatment_dict)
    int_inference = VariableElimination(int_net)
    truth = int_inference.query(list(outcome_dict.keys())).get_value(
        **outcome_dict
    )

    return truth
